class CounterlabsSettingsController < ApplicationController

  layout 'counterlabs'
  before_action :confirm_logged_in

  def module
    @module = AppModule.find_by(module_name: 'counterlabs')
  end

  def toggle_module
    @module = AppModule.find_by(module_name: 'counterlabs')
    if @module.module_enabled
      @module.update_attributes(module_enabled: false)
      system('sudo service snmpd stop')
      render('module')
    else
      @module.update_attributes(module_enabled: true)
      system('sudo service snmpd start')
      render('module')
    end
  end

  def devices
    @counterlabs_devices = CounterlabsDevice.all
  end

  def new
    @device = CounterlabsDevice.new
  end

  def create
    @device = CounterlabsDevice.new(device_params)
    @device.app_module_id = AppModule.find_by(module_name: 'counterlabs').id
    if @device.save
      flash[:notice] = "Device '#{@device.ip_address}' created successfully."
      redirect_to(:action => 'devices')
    else
      render('new')
    end
  end

  def edit
    @counterlabs_devices = CounterlabsDevice
    @device = CounterlabsDevice.find(params[:id])
  end

  def update
    @device = CounterlabsDevice.find(params[:id])
    if @device.update_attributes(device_params)
      flash[:notice] = "Device '#{@device.ip_address}' updated successfully."
      redirect_to(:action => 'devices')
    else
      render('edit')
    end
  end

  def delete
    device = CounterlabsDevice.find(params[:id]).destroy
    flash[:notice] = "Device '#{device.ip_address}' removed successfully."
    redirect_to(:action => 'devices')
  end


  private
  def device_params
    params.require(:device).permit(:ip_address, :description)
  end

end
