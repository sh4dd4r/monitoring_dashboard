class AccessController < ApplicationController

  layout 'access'

  def login

  end

  def attempt_login
    if params[:username].present? && params[:password].present?
      found_user = User.where(:username => params[:username]).first
      if found_user
        authorized_user = found_user.authenticate(params[:password])
      end
    end
    if authorized_user
      session[:user_id] = authorized_user.id
      session[:username] = authorized_user.username
      redirect_to(:controller => 'dashboard', :action => 'index')
    else
      flash[:notice] = 'Invalid username/password combination.'
      redirect_to(:action => 'login')
    end
  end

  def logout
    session[:user_id] = nil
    session[:username] = nil
    redirect_to(:controller => 'dashboard', :action => 'index')
  end

end
