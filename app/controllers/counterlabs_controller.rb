class CounterlabsController < ApplicationController

  layout 'counterlabs'

  def overview
    @counterlabs_devices = CounterlabsDevice
    @counterlabs_records = CounterlabsRecord
  end

  def history
    @counterlabs_devices = CounterlabsDevice
    @counterlabs_records = CounterlabsRecord
  end

  def export
    @counterlabs_devices = CounterlabsDevice
    @counterlabs_records = CounterlabsRecord
    respond_to do |format|
      format.html
      format.csv {
        start_date = Date.civil(params[:range][:'start_date(1i)'].to_i, params[:range][:'start_date(2i)'].to_i, params[:range][:'start_date(3i)'].to_i)
        end_date = Date.civil(params[:range][:'end_date(1i)'].to_i, params[:range][:'end_date(2i)'].to_i, params[:range][:'end_date(3i)'].to_i)
        filename = "COUNTERLABS-#{CounterlabsDevice.find_by(id: params[:id]).ip_address}_#{start_date}--#{end_date}.csv"
        export = CounterlabsRecord.where(clabs_device_id: params[:id], created_at: start_date..end_date)
        send_data(export.to_csv, type: 'text/csv; charset=utf-8; header=present', filename: filename)
      }
    end
  end

end
