class TehulabsSettingsController < ApplicationController

  layout 'tehulabs'
  before_action :confirm_logged_in

  def module
    @module = AppModule.find_by(module_name: 'tehulabs')
  end

  def toggle_module
    @module = AppModule.find_by(module_name: 'tehulabs')
    if @module.module_enabled
      @module.update_attributes(module_enabled: false)
      system('sudo stop md-clockwork')
      render('module')
    else
      @module.update_attributes(module_enabled: true)
      system('sudo start md-clockwork')
      render('module')
    end
  end

  def restart_module
    @module = AppModule.find_by(module_name: 'tehulabs')
    system('sudo restart md-clockwork')
    render('module')
  end

  def devices
    @tehulabs_devices = TehulabsDevice.all
  end

  def new
    @device = TehulabsDevice.new
  end

  def create
    @device = TehulabsDevice.new(device_params)
    @device.app_module_id = AppModule.find_by(module_name: 'tehulabs').id
    if @device.save
      flash[:notice] = "Device '#{@device.ip_address}' created successfully."
      redirect_to(:action => 'devices')
    else
      render('new')
    end
  end

  def edit
    @tehulabs_devices = TehulabsDevice
    @device = TehulabsDevice.find(params[:id])
  end

  def update
    @device = TehulabsDevice.find(params[:id])
    if @device.update_attributes(device_params)
      flash[:notice] = "Device '#{@device.ip_address}' updated successfully."
      redirect_to(:action => 'devices')
    else
      render('edit')
    end
  end

  def delete
    device = TehulabsDevice.find(params[:id]).destroy
    flash[:notice] = "Device '#{device.ip_address}' removed successfully."
    redirect_to(:action => 'devices')
  end


  private
  def device_params
    params.require(:device).permit(:ip_address, :description, :interval, :sma_period, :temperature_correction,
                                   :humidity_correction)
  end

end
