class TehulabsController < ApplicationController

  layout 'tehulabs'

  def overview
    @tehulabs_devices = TehulabsDevice
    @tehulabs_records = TehulabsRecord
  end

  def history
    @tehulabs_devices = TehulabsDevice
    @tehulabs_records = TehulabsRecord
  end

  def export
    @tehulabs_devices = TehulabsDevice
    @tehulabs_records = TehulabsRecord
    respond_to do |format|
      format.html
      format.csv {
        start_date = Date.civil(params[:range][:'start_date(1i)'].to_i, params[:range][:'start_date(2i)'].to_i, params[:range][:'start_date(3i)'].to_i)
        end_date = Date.civil(params[:range][:'end_date(1i)'].to_i, params[:range][:'end_date(2i)'].to_i, params[:range][:'end_date(3i)'].to_i)
        filename = "TEHULABS-#{TehulabsDevice.find_by(id: params[:id]).ip_address}_#{start_date}--#{end_date}.csv"
        export = TehulabsRecord.where(tlabs_device_id: params[:id], created_at: start_date..end_date)
        send_data(export.to_csv, type: 'text/csv; charset=utf-8; header=present', filename: filename)
      }
    end

  end

end
