class DashboardController < ApplicationController

  layout 'dashboard'

  def index
    @app_modules = AppModule.all
    @tehulabs_devices = TehulabsDevice.all
    @counterlabs_devices = CounterlabsDevice.all
  end

end
