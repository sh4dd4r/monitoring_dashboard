class TehulabsWorker
  @queue = :tehulabs

  def self.perform(ip)
    Rails.logger.info "Executing: TLABS - #{ip}"
    TehulabsRecord.receive(ip)
  end
end
