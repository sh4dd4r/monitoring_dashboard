
class CounterlabsWorker
	@queue = :counterlabs

  def self.perform(ip, time)
		Rails.logger.info "Executing: CLABS - #{ip} - #{time}}"
		ClabsRecord.receive(ip, time)
	end
end
