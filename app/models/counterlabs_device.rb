# == Schema Information
#
# Table name: counterlabs_devices
#
#  id            :integer          not null, primary key
#  app_module_id :integer          not null
#  ip_address    :string(30)       not null
#  description   :string(100)      not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  deleted_at    :datetime         not null
#

class CounterlabsDevice < ActiveRecord::Base

  belongs_to :app_module
  has_many :counterlabs_records
  acts_as_paranoid

end
