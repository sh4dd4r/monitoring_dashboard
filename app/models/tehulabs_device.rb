# == Schema Information
#
# Table name: tehulabs_devices
#
#  id                     :integer          not null, primary key
#  app_module_id          :integer          not null
#  ip_address             :string(30)       not null
#  description            :string(100)      not null
#  interval               :time             default("00:00:00"), not null
#  sma_period             :integer          default("0"), not null
#  temperature_correction :float(24)        default("0"), not null
#  humidity_correction    :float(24)        default("0"), not null
#  device_online          :boolean          default("1"), not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  deleted_at             :datetime         not null
#

class TehulabsDevice < ActiveRecord::Base

  belongs_to :app_module
  has_many :tehulabs_records
  acts_as_paranoid

  IP_REGEX = /\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(:\d{1,5}\b)?/i
  validates :ip_address, presence: true,
            uniqueness: true,
            length: {maximum: 30},
            format: {with: IP_REGEX}

  validates :description, presence: true,
            length: {maximum: 100}

  validates :interval, presence: true

  validates :sma_period, presence: true,
            numericality: {only_integer: true, less_than: 60}

  validates :temperature_correction, presence: true,
            numericality: true

  validates :humidity_correction, presence: true,
            numericality: true

end
