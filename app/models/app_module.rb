# == Schema Information
#
# Table name: app_modules
#
#  id             :integer          not null, primary key
#  module_name    :string(30)       not null
#  module_enabled :boolean          default("1"), not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class AppModule < ActiveRecord::Base

  has_many :tehulabs_devices
  has_many :counterlabs_devices

end
