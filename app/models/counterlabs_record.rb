# == Schema Information
#
# Table name: counterlabs_records
#
#  id                    :integer          not null, primary key
#  counterlabs_device_id :integer          not null
#  created_at            :datetime         not null
#

class CounterlabsRecord < ActiveRecord::Base

  belongs_to :counterlabs_device

end
