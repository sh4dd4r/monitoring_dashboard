# == Schema Information
#
# Table name: tehulabs_records
#
#  id                 :integer          not null, primary key
#  tehulabs_device_id :integer          not null
#  temperature        :float(24)        not null
#  temperature_sma    :float(24)        not null
#  humidity           :float(24)        not null
#  humidity_sma       :float(24)        not null
#  relay1             :boolean          default("0"), not null
#  relay2             :boolean          default("0"), not null
#  created_at         :datetime         not null
#

class TehulabsRecord < ActiveRecord::Base

  belongs_to :tehulabs_device
  require 'nokogiri'

  scope :scope_10minutes, -> (id) {where(tehulabs_device_id: id, created_at: (Time.now-10.minutes)..Time.now)}
  scope :scope_1hour, -> (id) {where(tehulabs_device_id: id, created_at: (Time.now-1.hour)..Time.now)}
  scope :scope_12hours, -> (id) {where(tehulabs_device_id: id, created_at: (Time.now-12.hours)..Time.now)}
  scope :scope_1day, -> (id) {where(tehulabs_device_id: id, created_at: (Time.now-1.day)..Time.now)}
  scope :scope_7days, -> (id) {where(tehulabs_device_id: id, created_at: (Time.now-7.days)..Time.now)}

  def self.receive(ip)
    # For development environment only.
    exit! unless AppModule.find_by(module_name: 'tehulabs').module_enabled
    #
    process(ip)
  end

  private

  def self.process(ip)
    device = TehulabsDevice.find_by(ip_address: ip)
    parsed_values = parse(get_status_xml(device))
    commit_record(device, parsed_values)
  end

  def self.get_status_xml(device)
    url = URI.parse("http://#{device.ip_address}/status.xml")
    begin
      response = Net::HTTP.get_response(url)
      if response.code != '200'
        raise
      end
    rescue
      device_offline(device)
      exit 1
    end
    return response
  end

  def self.parse(xml)
    xml_doc = Nokogiri::XML(xml.body)
    parsed_values = {temperature: parser(xml_doc, 'Temperature1'),
                     humidity: parser(xml_doc, 'Humidity1'),
                     relay1: parser(xml_doc, 'Relay1'),
                     relay2: parser(xml_doc, 'Relay2')}
    return parsed_values
  end

  def self.parser(xml_document, tag)
    begin
      value = xml_document.at_xpath("//Monitor//#{tag}").content[0...-2].to_f
    rescue
      Rails.logger.debug "Tehulabs module: #{tag} value not found.}"
    end
    return value
  end

  def self.commit_record(device, parsed_values)
    device_online(device) unless device.device_online
    new_record(device, parsed_values)
  end

  def self.new_record(device, parsed_values)
    record = TehulabsRecord.new
    moving_average = moving_average(device, parsed_values)
    record.attributes = {tehulabs_device_id: device.id,
                         temperature: parsed_values[:temperature] + device.temperature_correction,
                         temperature_sma: moving_average[:temperature],
                         humidity: parsed_values[:humidity] + device.humidity_correction,
                         humidity_sma: moving_average[:humidity],
                         relay1: relay_condition(parsed_values[:relay1]),
                         relay2: relay_condition(parsed_values[:relay2])}
    record.save
  end

  def self.moving_average(device, parsed_values)
    past_records = TehulabsRecord.where(tehulabs_device_id: device.id).last(device.sma_period-1)
    past_values = {temperature: 0.0, humidity: 0.0}
    past_records.each do |record|
      past_values[:temperature] += record.temperature
      past_values[:humidity] += record.humidity
    end
    moving_average = {temperature: moving_average_calculation(device, past_values[:temperature], parsed_values[:temperature]),
                      humidity: moving_average_calculation(device, past_values[:humidity], parsed_values[:humidity])}
    return moving_average
  end

  def self.moving_average_calculation(device, past_values, current_value)
    moving_average = ((past_values + current_value) / device.sma_period).round(1)
    return moving_average
  end

  def self.relay_condition(relay_status)
    return true if relay_status == 'ON'
    return false
  end

  def self.device_offline(device)
    device.update_attributes(device_online: false)
    Rails.logger.debug "Tehulabs module: Device #{device.ip_address} offline.}"
  end

  def self.device_online(device)
    device.update_attributes(device_online: true)
  end

end
