class CreateTehulabsDevices < ActiveRecord::Migration
  def change
    create_table :tehulabs_devices do |t|
      t.references :app_module, null: false
      t.string 'ip_address', limit: 30, null: false
      t.string 'description', limit: 100, null: false
      t.time 'interval', default: '00:00:00', null: false
      t.integer 'sma_period', default: 0, null: false
      t.float 'temperature_correction', default: 0, null: false
      t.float 'humidity_correction', default: 0, null: false
      t.boolean 'device_online', default: true, null: false
      t.timestamps null: false
      t.datetime 'deleted_at'
    end
    add_index('tehulabs_devices', 'app_module_id')
    add_index('tehulabs_devices', 'ip_address')
  end
end
