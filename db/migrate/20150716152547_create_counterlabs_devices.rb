class CreateCounterlabsDevices < ActiveRecord::Migration
  def change
    create_table :counterlabs_devices do |t|
      t.references :app_module, null: false
      t.string 'ip_address', limit: 30, null: false
      t.string 'description', limit: 100, null: false
      t.timestamps null: false
      t.datetime 'deleted_at', null: false
    end
    add_index('counterlabs_devices', 'app_module_id')
    add_index('counterlabs_devices', 'ip_address')
  end
end
