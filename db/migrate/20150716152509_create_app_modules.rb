class CreateAppModules < ActiveRecord::Migration
  def change
    create_table :app_modules do |t|
      t.string 'module_name', limit: 30, null: false
      t.boolean 'module_enabled', default: true, null: false
      t.timestamps null: false
    end
  end
end
