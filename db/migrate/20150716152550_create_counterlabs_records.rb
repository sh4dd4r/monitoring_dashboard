class CreateCounterlabsRecords < ActiveRecord::Migration
  def change
    create_table :counterlabs_records do |t|
      t.references :counterlabs_device, null: false
      t.datetime 'created_at'
    end
    add_index('counterlabs_records', 'counterlabs_device_id')
    add_index('counterlabs_records', 'created_at')
  end
end
