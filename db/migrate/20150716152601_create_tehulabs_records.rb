class CreateTehulabsRecords < ActiveRecord::Migration
  def change
    create_table :tehulabs_records do |t|
      t.references :tehulabs_device, null: false
      t.float 'temperature', default: 0, null: false
      t.float 'temperature_sma', default: 0, null: false
      t.float 'humidity', default: 0, null: false
      t.float 'humidity_sma', default: 0, null: false
      t.boolean 'relay1', default: false, null: false
      t.boolean 'relay2', default: false, null: false
      t.datetime 'created_at', null: false
    end
    add_index('tehulabs_records', 'tehulabs_device_id')
    add_index('tehulabs_records', 'created_at')
  end
end
