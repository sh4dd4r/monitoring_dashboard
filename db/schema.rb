# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150716152612) do

  create_table "app_modules", force: :cascade do |t|
    t.string   "module_name",    limit: 30,                null: false
    t.boolean  "module_enabled",            default: true, null: false
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  create_table "counterlabs_devices", force: :cascade do |t|
    t.integer  "app_module_id", limit: 4,   null: false
    t.string   "ip_address",    limit: 30,  null: false
    t.string   "description",   limit: 100, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.datetime "deleted_at",                null: false
  end

  add_index "counterlabs_devices", ["app_module_id"], name: "index_counterlabs_devices_on_app_module_id", using: :btree
  add_index "counterlabs_devices", ["ip_address"], name: "index_counterlabs_devices_on_ip_address", using: :btree

  create_table "counterlabs_records", force: :cascade do |t|
    t.integer  "counterlabs_device_id", limit: 4, null: false
    t.datetime "created_at"
  end

  add_index "counterlabs_records", ["counterlabs_device_id"], name: "index_counterlabs_records_on_counterlabs_device_id", using: :btree
  add_index "counterlabs_records", ["created_at"], name: "index_counterlabs_records_on_created_at", using: :btree

  create_table "tehulabs_devices", force: :cascade do |t|
    t.integer  "app_module_id",          limit: 4,                                   null: false
    t.string   "ip_address",             limit: 30,                                  null: false
    t.string   "description",            limit: 100,                                 null: false
    t.time     "interval",                           default: '2000-01-01 00:00:00', null: false
    t.integer  "sma_period",             limit: 4,   default: 0,                     null: false
    t.float    "temperature_correction", limit: 24,  default: 0.0,                   null: false
    t.float    "humidity_correction",    limit: 24,  default: 0.0,                   null: false
    t.boolean  "device_online",                      default: true,                  null: false
    t.datetime "created_at",                                                         null: false
    t.datetime "updated_at",                                                         null: false
    t.datetime "deleted_at"
  end

  add_index "tehulabs_devices", ["app_module_id"], name: "index_tehulabs_devices_on_app_module_id", using: :btree
  add_index "tehulabs_devices", ["ip_address"], name: "index_tehulabs_devices_on_ip_address", using: :btree

  create_table "tehulabs_records", force: :cascade do |t|
    t.integer  "tehulabs_device_id", limit: 4,                  null: false
    t.float    "temperature",        limit: 24, default: 0.0,   null: false
    t.float    "temperature_sma",    limit: 24, default: 0.0,   null: false
    t.float    "humidity",           limit: 24, default: 0.0,   null: false
    t.float    "humidity_sma",       limit: 24, default: 0.0,   null: false
    t.boolean  "relay1",                        default: false, null: false
    t.boolean  "relay2",                        default: false, null: false
    t.datetime "created_at",                                    null: false
  end

  add_index "tehulabs_records", ["created_at"], name: "index_tehulabs_records_on_created_at", using: :btree
  add_index "tehulabs_records", ["tehulabs_device_id"], name: "index_tehulabs_records_on_tehulabs_device_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "username",        limit: 255, null: false
    t.string   "password_digest", limit: 255, null: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

end
