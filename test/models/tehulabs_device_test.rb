# == Schema Information
#
# Table name: tehulabs_devices
#
#  id                     :integer          not null, primary key
#  app_module_id          :integer          not null
#  ip_address             :string(30)       not null
#  description            :string(100)      not null
#  interval               :time             default("00:00:00"), not null
#  sma_period             :integer          default("0"), not null
#  temperature_correction :float(24)        default("0"), not null
#  humidity_correction    :float(24)        default("0"), not null
#  device_online          :boolean          default("1"), not null
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  deleted_at             :datetime         not null
#

require 'test_helper'

class TehulabsDeviceTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
