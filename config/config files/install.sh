#!/bin/bash

sudo rm /etc/snmp/snmptrapd.conf
sudo rm /etc/snmp/snmptt.ini
sudo rm /etc/snmp/snmptt.conf
sudo rm /etc/default/snmpd
sudo ln -sf /var/www/moni_dashboard/config/snmptrapd.conf /etc/snmp/
sudo ln -sf /var/www/moni_dashboard/config/snmptt.ini /etc/snmp/
sudo ln -sf /var/www/moni_dashboard/config/snmptt.conf /etc/snmp/
sudo ln -sf /var/www/moni_dashboard/config/snmpd /etc/default/
sudo service snmpd restart
sudo service snmptt restart

sudo rm /etc/nginx/nginx.conf
sudo rm /etc/nginx/sites-enabled/default
sudo ln -sf /var/www/moni_dashboard/config/nginx.conf /etc/nginx/
sudo ln -sf /var/www/moni_dashboard/config/md_nginx.conf /etc/nginx/sites-enabled/
sudo service nginx restart