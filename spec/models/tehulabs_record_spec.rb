require 'rails_helper'

RSpec.describe TehulabsRecord, type: :model do

  context 'db' do
    context 'indexes' do
      it {should have_db_index(:tehulabs_device_id)}
      it {should have_db_index(:created_at)}
    end
    context 'columns' do
      it {should have_db_column(:temperature).of_type(:float).with_options(null: false)}
      it {should have_db_column(:temperature_sma).of_type(:float).with_options(null: false)}
      it {should have_db_column(:humidity).of_type(:float).with_options(null: false)}
      it {should have_db_column(:humidity_sma).of_type(:float).with_options(null: false)}
      it {should have_db_column(:relay1).of_type(:boolean).with_options(null: false, default: false)}
      it {should have_db_column(:relay2).of_type(:boolean).with_options(null: false, default: false)}
    end
  end

  context 'scopes' do

    describe '.relay_condition' do
      it 'return true if parameter equals ON' do
        expect(TehulabsRecord.relay_condition('ON')).to be true
      end
      it 'return false if parameter is anything other than ON' do
        expect(TehulabsRecord.relay_condition('OFF')).to be false
        expect(TehulabsRecord.relay_condition('---')).to be false
        expect(TehulabsRecord.relay_condition('0')).to be false
      end
    end

    describe '.device_offline' do
      it 'sets the device_online parameter to false if initial value is true' do
        device = TehulabsDevice.new(app_module_id: 1,
                                        ip_address: '192.168.1.1',
                                        description: 'Test 1',
                                        interval: '00:01:00',
                                        sma_period: 5,
                                        temperature_correction: 0,
                                        humidity_correction: 0,
                                        device_online: true)
        TehulabsRecord.device_offline(device)
        expect(device.device_online).to be false
      end
      it 'sets the device_online parameter to false if initial value is false' do
        device = TehulabsDevice.new(app_module_id: 1,
                                        ip_address: '192.168.1.1',
                                        description: 'Test 1',
                                        interval: '00:01:00',
                                        sma_period: 5,
                                        temperature_correction: 0,
                                        humidity_correction: 0,
                                        device_online: false)
        TehulabsRecord.device_offline(device)
        expect(device.device_online).to be false
      end
    end

    describe '.device_online' do
      it 'sets the device_online parameter to true if initial value is true' do
        device = TehulabsDevice.new(app_module_id: 1,
                                        ip_address: '192.168.1.1',
                                        description: 'Test 1',
                                        interval: '00:01:00',
                                        sma_period: 5,
                                        temperature_correction: 0,
                                        humidity_correction: 0,
                                        device_online: true)
        TehulabsRecord.device_online(device)
        expect(device.device_online).to be true
      end
      it 'sets the device_online parameter to true if initial value is false' do
        device = TehulabsDevice.new(app_module_id: 1,
                                        ip_address: '192.168.1.1',
                                        description: 'Test 1',
                                        interval: '00:01:00',
                                        sma_period: 5,
                                        temperature_correction: 0,
                                        humidity_correction: 0,
                                        device_online: false)
        TehulabsRecord.device_online(device)
        expect(device.device_online).to be true
      end
    end

  end

end