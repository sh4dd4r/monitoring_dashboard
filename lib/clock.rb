require 'clockwork'
require File.expand_path('../../config/boot',        __FILE__)
require File.expand_path('../../config/environment', __FILE__)

include Clockwork

TehulabsDevice.all.each do |device|
	min = (device.interval.hour * 60) + device.interval.min
	puts device.ip_address
	every(min.minute, "TEHULABS - #{device.ip_address}"){Resque.enqueue(TehulabsWorker, device.ip_address)}
end

